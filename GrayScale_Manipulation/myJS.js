
var img;

function ImgHandler(){
    var can = document.getElementById("can1");
    var imgIn = document.getElementById("imgIn");
    img = new SimpleImage(imgIn);
    img.drawTo(can);
}

function GrayScale(){
    for(var pixel of img.values()){
        var avg = (pixel.getRed() + pixel.getGreen() + pixel.getBlue()) / 3;
        pixel.setRed(avg);
        pixel.setGreen(avg);
        pixel.setBlue(avg);
    }
    var can = document.getElementById("can1");
    img.drawTo(can);
}
