

function CallerA(){
    Foo("yellow" , "blue" , 20, 50);
}

function CallerB(){
    Foo("green" , "red" , 80, 150);
}

function Foo( bckColor , inColor,  begin, end)
{
    const N = 10;
    var can = document.getElementById("can1");
    can.style.backgroundColor=bckColor;
    var ctx = can.getContext("2d");
    ctx.fillStyle = inColor;
    for(i=0; i < (end-begin)/N; ++i)
    {
        ctx.fillRect(begin + i*N, begin+ i*N, (end-begin)+ i*N , (end-begin) + i*N);
    }

}
