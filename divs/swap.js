
function SwapBckColor(){
    var d1 = document.getElementById("div1");
    var d2 = document.getElementById("div2");
    var temp = d1.className;
    d1.className = d2.className;
    d2.className = temp;
}

function SwapInnerHtml(){
    var d1 = document.getElementById("div1");
    var d2 = document.getElementById("div2");
    var temp = d1.innerHTML;
    d1.innerHTML = d2.innerHTML;
    d2.innerHTML = temp;
}

